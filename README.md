# Entry task

[题目链接](https://confluence.shopee.io/pages/viewpage.action?pageId=445746272)

## 总结

### entry task01（编写SPL申请贷款额度的测试用例）

针对评审提出的建议：
* 接口测试
* 数字的精确度
* 幂等
* 字段信息（唯一性）
* app入口的性能测试

### entry task02（实现gateway转发consumerloan、cashloan、risk的功能）

针对评审提出的建议：
- postman参数化
- 内存、io等监测
- 稳定性（测试时间为3分钟，那针对长期服务呢？）
- 加深对性能的理解（性能瓶颈）
- 监听器的禁用(会影响测试的性能，jmeter的文档有写)
- 分析、诊断（对于自动化测试文档的分析和诊断）
- 聚合报告的分析

## 01

***
[测试用例链接](https://docs.google.com/spreadsheets/d/1eY7qqjpPnqRUTfRNVTe9qwhlWcNfgsTUGkx5F9t3eLY/edit#gid=0)

## 02

***

### A.根据上述接口需求任选一种自已开发语言实现接口的功能，交付件要求：

1）实现接口的代码
在`demo`文件夹中，其中`gateway_server.py`用于启动网关，`consumer
loan.py`, `cashloan.py`, `risk.py`分别用于实现对应的api接口。

2）运行演示

### B.下载安装postman以newman工具，针对a实现的接口进行自动化用例脚本编写(包含结果的错误码和错误信息的检查)，并通过命令行调度自动脚本运行，生成测试报告。交付件要求：

1）自动化脚本

未开启服务
`scripts/entry_task_without_service.postman_collection.json`

开启服务后
`scripts/entry_task_with_service.postman_collection.json`

2）测试报告

`test_report/newman/`

3）演示自动化运行

4）梳理Postman接口自动化设计流程图
`entry task postman流程图.drawio.html`

### C.自选工具针对上述实现接口进行性能测试，测试对象为Gateway 服务，交付件要求：

1）性能测试脚本

`scripts/压测gateway.jmx`

2）性能测试结果报告

`entry-task/test_report/jmeter/test_report_dir`

3）演示性能测试执行
