# from gevent import monkey
# monkey.patch_all()
from flask import Flask, request, Response
import requests
# from gevent.pywsgi import WSGIServer
import json
from multiprocessing import cpu_count, Process
from aiohttp import ClientSession
import asyncio

host_conf = {
	"consumerloan": "http://127.0.0.1:5001/consumerloan/",
	"cashloan": "http://127.0.0.1:5002/cashloan/",
	"risk": "http://127.0.0.1:5003/risk/",
}

app = Flask(__name__)


async def one_task(service_name):
	req_data = {"cookie": request.args.get("cookie")}
	res = requests.get(host_conf[service_name], params=req_data)
	return res
	# res_gateway = {"Code": res.status_code, "Result": json.loads(res.text)["Result"] if res.status_code==200 else res.text}
	# raise ValueError
	# return Response(json.dumps(res_gateway),status=res.status_code, mimetype='application/json')
	# except Exception:
	#     return Response('{"Code":500, "Result":"Gateway Error"}',  mimetype='application/json')


async def creat_task(service_name):
	return await asyncio.create_task(one_task(service_name))


@app.route("/gateway/<service_name>/")
def gateway(service_name):
	try:
		res = asyncio.run(creat_task(service_name))
		return Response(res.text, status=res.status_code, mimetype='application/json')
	except Exception:
		return Response('{"Code":500, "Result":"Gateway Error"}', mimetype='application/json')


if __name__ == '__main__':
	app.run("0.0.0.0", 8081, threaded=True)
	# WSGIServer(("0.0.0.0", 80), app).serve_forever()
	# run(False)
	# http_server = HTTPServer(WSGIContainer(app)).listen(80, address="0.0.0.0")
	# # http_server.listen(port, address=bind)
	# # LOG.info('Listening on {}:{}'.format(bind, port))
	# ioloop.IOLoop.instance().start()
