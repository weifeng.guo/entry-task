# -*- coding: utf-8 -*-
'''
@Author: JacobOuch
@License:  
@Contact: 943411478@qq.com
@File: risk.py
@Time: 2022/5/13 15:21
@Software: PyCharm
@Description:
'''

"""运行程序：
uvicorn main:app --port 80 --reload
cd /Users/weifeng.guo/Desktop/entry_task/entry-task/demo
/Users/weifeng.guo/Desktop/entry_task/venv/bin/uvicorn consumerloan:app --port 5001 --reload
"""
from typing import Optional
from fastapi import FastAPI, Cookie, status
import time
from enum import Enum

import uvicorn

app = FastAPI()


@app.get("/")
def read_root():
	return {"msg": "Hello World)"}


@app.get("/consumerloan", status_code=status.HTTP_200_OK)
async def read_item(cookie_value: Optional[str] = Cookie(None)):
	if cookie_value:
		return {"Code": 200, "Result": {"Result": "ok", "Service": "consumerloan"}}
	else:
		return {"Code": 200, "Result": {"Result": "bad request, cookie required.", "Service": "consumerloan"}}


if __name__ == "__main__":
	uvicorn.run(app="15_cookies:app", host="127.0.0.1", port=5001, reload=True, debug=True)
