from gevent import monkey

monkey.patch_all()
from flask import Flask, request, Response
import requests
# from gevent.pywsgi import WSGIServer
import json

# from multiprocessing import cpu_count, Process
# from tornado.wsgi import WSGIContainer
# from tornado.httpserver import HTTPServer
# from tornado import ioloop

host_conf = {
	"consumerloan": "http://127.0.0.1:5001/consumerloan",
	"cashloan": "http://127.0.0.1:5002/cashloan",
	"risk": "http://127.0.0.1:5003/risk",
}

app = Flask(__name__)


@app.route("/gateway/<service_name>/")
def gateway(service_name):
	try:
		if not request.cookies.get('hello'):
		# if not request.args.get("cookie"):
			req_data = {"cookie": request.args.get("cookie")}
			res = requests.get(host_conf[service_name], params=req_data)
			res_gateway = {"Code": res.status_code,
			               "Result": json.loads(res.text)["Result"] if res.status_code == 200 else res.text}
		else:
			res = requests.get(host_conf[service_name])
			res_gateway = {"Code": 200, "Result": {"Result": "ok", "Service": "consumerloan"}}
			# {"Code": res.status_code,
			#                "Result": json.loads(res.text)["Result"] if res.status_code == 200 else res.text}

		# raise ValueError
		return Response(json.dumps(res_gateway), status=res.status_code, mimetype='application/json')
	except Exception:
		return Response('{"Code":501, "Result":"Gateway 可用，下游服务不可用"}', mimetype='application/json')


@app.route("/gateway")
# @app.route("/gateway/index")
def index():
	try:
		req_data = {"cookie": request.args.get("cookie")}
		try:
			res = requests.get("http://127.0.0.1:5002/cashloan/", params=req_data)
			res_gateway = {"Code": res.status_code,
			               "Result": json.loads(res.text)["Result"] if res.status_code == 200 else res.text}
			# raise ValueError
			return Response(json.dumps(res_gateway), status=res.status_code, mimetype='application/json')
		except Exception as e:
			raise Exception
	except Exception:
		return Response('{"Code":500, "Result":"Gateway Error"}', mimetype='application/json')


# def run(MULTI_PROCESS):
# 	if MULTI_PROCESS == False:
# 		WSGIServer(('0.0.0.0', 80), app).serve_forever()
# 	else:
# 		mulserver = WSGIServer(('0.0.0.0', 80), app)
# 		mulserver.start()
#
# 		def server_forever():
# 			mulserver.start_accepting()
# 			mulserver._stop_event.wait()
#
# 		for i in range(cpu_count()):
# 			p = Process(target=server_forever)
# 			p.start()


if __name__ == '__main__':
	app.run("0.0.0.0", 80)
# WSGIServer(("0.0.0.0", 80), app).serve_forever()
# run(True)
# http_server = HTTPServer(WSGIContainer(app)).listen(80, address="0.0.0.0")
# http_server.listen(port, address=bind)
# LOG.info('Listening on {}:{}'.format(bind, port))
# ioloop.IOLoop.instance().start()
