# -*- coding: utf-8 -*-
'''
@Author: JacobOuch
@License:  
@Contact: 943411478@qq.com
@File: risk.py
@Time: 2022/5/13 15:21
@Software: PyCharm
@Description:
'''

"""运行程序：
uvicorn main:app --port 80 --reload
cd /Users/weifeng.guo/Desktop/entry_task/entry-task/demo
/Users/weifeng.guo/Desktop/entry_task/venv/bin/uvicorn risk:app --port 5003 --reload
"""
from typing import Optional
from fastapi import FastAPI, Cookie, status
import time
from enum import Enum

import uvicorn

app = FastAPI()


@app.get("/")
def read_root():
	return {"msg": "Hello World)"}


class Service(str, Enum):
	consumerloan = "consumerloan"
	cashloan = "cashloan"
	risk = "risk"


@app.get("/risk")
def read_item():
	time.sleep(0.05)  # Risk服务在收到请求后，需要先延迟50ms，再处理请求。
	return {"Code":200, "Result":{"Result":"ok", "Service":"risk"}}


if __name__ == "__main__":
	uvicorn.run(app="15_cookies:app", host="127.0.0.1", port=5003, reload=True, debug=True)
