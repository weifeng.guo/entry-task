# -*- coding: utf-8 -*-
'''
@Author: JacobOuch
@License:  
@Contact: 943411478@qq.com
@File: test.py
@Time: 2022/5/13 10:51
@Software: PyCharm
@Description:
'''

from typing import Optional

from fastapi import Cookie, FastAPI

app = FastAPI()


@app.get("/items/")
async def read_items(ads_id: Optional[str] = Cookie(None)):
    return {"ads_id": ads_id}